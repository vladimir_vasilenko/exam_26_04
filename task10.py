# что успел, то закоммитил


# 10*. Реализуйте класс Purchase который хранит в себе список покупок
# и их итоговую стоимость. Создаваться объект класса может, например, так
# >> purchase = Purchase(10, 'pen', 'book', 'pencil')
# Можно красиво напечатать сумму покупок и что в корзине
# >> print(purchase)
# Получить кол-во покупок в корзине
# Сложить стоимость 2-х корзин
# >> purchase_1 + purchase_2
# Прибавить к итоговой стоимости корзины какое-то число(int, float)
# >> purchase + 1.2
# >> 1 + purchase
# Добавить в корзину товар
# >> ("mango", 1) + purchase1
# >> purchase + ("mango", 1)

class Purchase:
    def __init__(self, amount, *args: str):
        self.amount = amount
        self.args = args

    def print_goods(self):
        """ Можно красиво напечатать сумму покупок и что в корзине """
        print("{}".format("\n".join(self.args)))
        print(f'Amount: {self.amount}')

    def goods_count(self):
        """ Получить кол-во покупок в корзине """
        print(f'Count of goods: {len(self.args)}')

    def add_amount(self, additional):
        """ Прибавить к итоговой стоимости корзины (int, float) """
        self.amount += additional
        print(f'Current amount: {self.amount}')

    def add_new_goods(self, price, *goods):
        """ Добавить в корзину товар """
        self.amount += price
        self.args += goods


purchase1 = Purchase(10, 'pen', 'book', 'pencil')
purchase1.print_goods()
purchase1.goods_count()
purchase2 = Purchase(30, 'table', 'chair', 'sofa')
purchase1.add_amount(1.5)
purchase1.add_new_goods(10, 'banana')
purchase1.print_goods()
