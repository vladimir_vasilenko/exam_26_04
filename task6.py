# 6. Напишите функцию, преобразующую входную строку в выходную как в примерах,
# Пусть s = "abcdef...xyz", тогда вывод будет таким:
# f(s, 1) => "a"
# f(s, 2) => "aba"
# f(s, 3) => "abcba"
# f(s, 4) => "abcdcba"

import string
str1 = string.ascii_lowercase


def convert(str2, shift):
    converted_str = str2[:shift]
    converted_str += str2[:shift - 1][::-1]
    print(converted_str)


convert(str1, 3)
