# 7. Реализуйте функцию которая умеет работать с файлами
# (читать из заданного файла, записывать в заданный файл).
# Программа считает количество строк, слов и букв в заданном файле и дописывает
# эту информацию обратно в файл, так же выводит эту информацию на экран.

def work_with_file(file_name: str):
    line_counter = 0
    with open(file_name, 'r') as my_file:
        for line in my_file:
            line_counter += 1
        print(f'Count of lines: {line_counter}')

    with open(file_name, 'r') as my_file:
        words = [word for line in my_file
                 for word in line.strip().split()]

        print(f"Count of words: {len(words)}")

    with open(file_name, 'r') as my_file:
        chars = sum(len(word) for word in my_file)
        print(f"Count of symbols: {chars}")

    with open(file_name, 'a') as my_file:
        my_file.write(f'\nCount of lines: {line_counter}')
        my_file.write(f'\nCount of words: {len(words)}')
        my_file.write(f"\nCount of symbols: {chars}")
