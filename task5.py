# 5. Напишите функцию, которая принимает строку и возвращает
# кол-во заглавных и строчных букв.
# 'The quick Brow Fox' =>
# Upper case characters: 3
# Lower case сharacters: 12

str1 = 'The quick Brow Fox'
upper_case = 0
lower_case = 0
for i in str1:
    if i.isupper():
        upper_case += 1
    elif i.islower():
        lower_case += 1
print(f'Upper case characters: {upper_case}\n'
      f'Lower case сharacters: {lower_case}')
