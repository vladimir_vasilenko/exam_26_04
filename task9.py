# 9. Создайте 3 класса: Animal, Cow, Dog.
# Классы Cow, Dog наследуются от класса Animal.
# У всех классов есть методы: swim, say. Дополните методы родительского класса.
#
# Например, метод родительского класса say печатает:
# >> print(f"{self.type} wants to say smth")
# Метод дочернего класса say печатает:
# >> print(f"{self.type} {self.name} says meow")
#
# >> cat = Cat("Vasya")
# >> cat.say()
#
# >> Cat wants to say smth
# >> Cat Vasya says meow

class Animal:

    def swim(self):
        print(f'{self.type} wants to swim')

    # я подозреваю, что в родительском классе так же метод должен называться
    # say(), но я все таки не знаю\не помню как это правильно реализовать.
    # если это так, то буду благодарен за объяснительный комментарий :)
    def say_smth(self):
        print(f'{self.type} wants to say smth')


class Cow(Animal):
    type = 'Cow'

    def __init__(self, name):
        self.name = name

    def say(self):
        print(f'{self.type} {self.name} wants to say muuu')


class Dog(Animal):
    type = 'Dog'

    def __init__(self, name):
        self.name = name

    def say(self):
        print(f'{self.type} {self.name} wants to bark')


cow = Cow('burenka')
dog = Dog('Sharik')
cow.say_smth()
cow.say()
dog.say_smth()
dog.say()
